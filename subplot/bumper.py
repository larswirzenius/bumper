import logging
import os
import shlex


def install_bumper(ctx):
    runcmd_prepend_to_path = globals()["runcmd_prepend_to_path"]
    srcdir = globals()["srcdir"]

    # Add the directory with built Rust binaries to the path.
    runcmd_prepend_to_path(ctx, dirname=os.path.join(srcdir, "target", "debug"))


def chmod_exec(ctx, filename=None):
    os.chmod(filename, 0o755)


def git_init_and_commit_everything(ctx, dirname=None):
    runcmd_run = globals()["runcmd_run"]
    runcmd_exit_code_is_zero = globals()["runcmd_exit_code_is_zero"]

    runcmd_run(
        ctx, ["git", "config", "--global", "user.email", "me@example.com", dirname]
    )
    runcmd_exit_code_is_zero(ctx)

    runcmd_run(
        ctx, ["git", "config", "--global", "user.name", "J. Random Hacker", dirname]
    )
    runcmd_exit_code_is_zero(ctx)

    runcmd_run(ctx, ["git", "init", dirname])
    runcmd_exit_code_is_zero(ctx)

    runcmd_run(ctx, ["git", "add", "."], cwd=dirname)
    runcmd_exit_code_is_zero(ctx)

    runcmd_run(ctx, ["git", "commit", "-minitial"], cwd=dirname)
    runcmd_exit_code_is_zero(ctx)


def remember_HEAD(ctx, dirname=None, varname=None):
    runcmd_run = globals()["runcmd_run"]
    runcmd_get_stdout = globals()["runcmd_get_stdout"]

    runcmd_run(ctx, ["git", "rev-parse", "HEAD"], cwd=dirname)
    runcmd_exit_code_is_zero = globals()["runcmd_exit_code_is_zero"]
    runcmd_exit_code_is_zero(ctx)

    ctx[varname] = runcmd_get_stdout(ctx).strip()


def run_command_in_directory(ctx, dirname=None, argv0=None, args=None):
    runcmd_run = globals()["runcmd_run"]
    runcmd_exit_code_is_zero = globals()["runcmd_exit_code_is_zero"]

    argv = [shlex.quote(argv0)] + shlex.split(args)
    runcmd_run(ctx, argv, cwd=dirname)
    runcmd_exit_code_is_zero(ctx)


def try_to_run_command_in_directory(ctx, dirname=None, argv0=None, args=None):
    runcmd_run = globals()["runcmd_run"]

    argv = [shlex.quote(argv0)] + shlex.split(args)
    runcmd_run(ctx, argv, cwd=dirname)


def only_these_files_exist_in(ctx, filenames=None, dirname=None):
    assert_eq = globals()["assert_eq"]
    expect = list(sorted(x for x in filenames.replace(",", "").split() if x != "and"))
    actual = list(sorted(os.listdir(dirname)))
    logging.debug(f"expect files; {expect}")
    logging.debug(f"actual files; {actual}")
    assert_eq(expect, actual)


def git_tag_exists(ctx, dirname=None, tag=None):
    runcmd_run = globals()["runcmd_run"]
    runcmd_get_stdout = globals()["runcmd_get_stdout"]
    runcmd_exit_code_is_zero = globals()["runcmd_exit_code_is_zero"]

    logging.debug(f"expecting tag {tag} to exist")
    runcmd_run(ctx, ["git", "show", "--raw", tag], cwd=dirname)
    runcmd_exit_code_is_zero(ctx)
    output = runcmd_get_stdout(ctx)
    logging.debug(f"tag: {output!r}")
    assert output.startswith(f"tag {tag}\n")


def git_working_tree_is_clean(ctx, dirname=None):
    runcmd_run = globals()["runcmd_run"]
    runcmd_get_stdout = globals()["runcmd_get_stdout"]
    runcmd_exit_code_is_zero = globals()["runcmd_exit_code_is_zero"]
    assert_eq = globals()["assert_eq"]

    runcmd_run(ctx, ["git", "status", "--porcelain"], cwd=dirname)
    runcmd_exit_code_is_zero(ctx)
    output = runcmd_get_stdout(ctx)
    assert_eq(output, "")


def file_is_newer_than_other_file(ctx, file1=None, file2=None):
    assert os.path.getmtime(file1) > os.path.getmtime(file2)


def file_is_in_git(ctx, filename=None):
    runcmd_run = globals()["runcmd_run"]
    runcmd_exit_code_is_zero = globals()["runcmd_exit_code_is_zero"]

    dirname = os.path.dirname(filename) or "."
    basename = os.path.basename(filename)
    runcmd_run(ctx, ["git", "blame", basename], cwd=dirname)
    runcmd_exit_code_is_zero(ctx)
