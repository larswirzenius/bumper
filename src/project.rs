use crate::debian::Debian;
use crate::errors::BumperError;
use crate::python::Python;
use crate::rust::Rust;
use log::{debug, info};
use std::path::Path;

pub enum ProjectKind {
    Rust(Rust),
    Debian(Debian),
    Python(Python),
}

impl ProjectKind {
    pub fn detect<P: AsRef<Path>>(dirname: P) -> Result<Vec<ProjectKind>, BumperError> {
        let dirname = dirname.as_ref();
        let mut kinds = vec![];

        debug!("detecting kinds of project in {}", dirname.display());

        if let Ok(p) = Rust::new(dirname) {
            kinds.push(ProjectKind::Rust(p));
        }

        if let Ok(p) = Debian::new(dirname) {
            kinds.push(ProjectKind::Debian(p));
        }

        if let Ok(p) = Python::new(dirname) {
            kinds.push(ProjectKind::Python(p));
        }

        if kinds.is_empty() {
            Err(BumperError::UnknownProjectKind(dirname.to_path_buf()))
        } else {
            for kind in kinds.iter() {
                info!("{} project in {}", kind.desc(), dirname.display());
            }
            Ok(kinds)
        }
    }

    pub fn desc(&self) -> &'static str {
        match self {
            Self::Debian(_) => "Debian package",
            Self::Python(_) => "Python",
            Self::Rust(_) => "Rust",
        }
    }

    pub fn name(&mut self) -> Result<String, BumperError> {
        match self {
            Self::Debian(x) => x.name(),
            Self::Python(x) => x.name(),
            Self::Rust(x) => x.name(),
        }
    }

    pub fn set_version(&mut self, version: &str) -> Result<String, BumperError> {
        Ok(match self {
            Self::Rust(ref mut rust) => rust.set_version(version)?,
            Self::Debian(ref mut debian) => debian.set_version(version)?,
            Self::Python(ref mut python) => python.set_version(version)?,
        })
    }
}
