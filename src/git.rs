use crate::errors::BumperError;
use log::debug;
use std::path::Path;
use std::process::Command;

pub fn tag<P: AsRef<Path>>(dirname: P, tag_name: &str, msg: &str) -> Result<(), BumperError> {
    git(dirname.as_ref(), &["tag", "-am", msg, tag_name])?;
    Ok(())
}

pub fn commit<P: AsRef<Path>>(dirname: P, msg: &str) -> Result<(), BumperError> {
    git(dirname.as_ref(), &["commit", "-am", msg])?;
    Ok(())
}

fn git(dirname: &Path, args: &[&str]) -> Result<(), BumperError> {
    debug!("git {:?} in {}", args, dirname.display());
    let output = Command::new("git")
        .args(args)
        .current_dir(dirname)
        .output()
        .map_err(|err| {
            let args = args.iter().map(|s| s.to_string()).collect();
            BumperError::GitInvoke(dirname.to_path_buf(), args, err)
        })?;
    debug!("git exit code was {:?}", output.status.code());
    if !output.status.success() {
        let args = args.iter().map(|s| s.to_string()).collect();
        let stderr = String::from_utf8_lossy(&output.stderr).into_owned();
        return Err(BumperError::Git(dirname.to_path_buf(), args, stderr));
    }
    Ok(())
}
