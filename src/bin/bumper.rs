use bumper::errors::BumperError;
use bumper::git;
use bumper::project::ProjectKind;
use bumper::tag::Tag;
use log::{debug, error};
use std::path::{Path, PathBuf};
use std::process::exit;
use structopt::StructOpt;

fn main() {
    if let Err(err) = bumper() {
        error!("{}", err);
        exit(1);
    }
}

fn bumper() -> Result<(), BumperError> {
    pretty_env_logger::init_custom_env("BUMPER_LOG");
    debug!("Bumper starts");
    let opt = Opt::from_args();

    let cwd = abspath(".")?;
    println!("Setting version for project in {}", cwd.display());

    let mut kinds = ProjectKind::detect(&cwd)?;
    if kinds.is_empty() {
        return Err(BumperError::UnknownProjectKind(cwd));
    }
    let name = kinds[0].name()?;

    for kind in kinds.iter_mut() {
        let version = kind.set_version(&opt.version)?;
        println!("{} project set to {}", kind.desc(), version);
    }

    let msg = format!("Release version {}", opt.version);
    git::commit(".", &msg)?;

    let tag = Tag::new(&opt.tag)?;
    let tag = tag.apply(&name, &opt.version);
    println!("release tag: {}", tag);
    git::tag(".", &tag, &msg)?;
    debug!("Bumper ends OK");
    Ok(())
}

fn abspath<P: AsRef<Path>>(path: P) -> Result<PathBuf, BumperError> {
    let path = path.as_ref();
    path.to_path_buf()
        .canonicalize()
        .map_err(|err| BumperError::AbsPath(path.to_path_buf(), err))
}

#[derive(Debug, StructOpt)]
struct Opt {
    #[structopt(long, default_value = "v%v")]
    tag: String,

    #[structopt(help = "version number of new release")]
    version: String,
}
