use crate::errors::BumperError;
use log::debug;
use std::path::{Path, PathBuf};
use std::process::Command;

pub struct Debian {
    dirname: PathBuf,
}

impl Debian {
    pub fn new(dirname: &Path) -> Result<Self, BumperError> {
        let changelog = dirname.join("debian/changelog");
        debug!("does {} exist? {}", changelog.display(), changelog.exists());
        if changelog.exists() {
            return Ok(Self {
                dirname: dirname.to_path_buf(),
            });
        }
        Err(BumperError::UnknownProjectKind(dirname.to_path_buf()))
    }

    pub fn name(&self) -> Result<String, BumperError> {
        let output = Command::new("dpkg-parsechangelog")
            .arg("-SSource")
            .current_dir(&self.dirname)
            .output()
            .map_err(|err| BumperError::ParseChangelogInvoke(self.dirname.to_path_buf(), err))?;
        if output.status.success() {
            let name = String::from_utf8_lossy(&output.stdout).into_owned();
            Ok(name.trim_end().to_string())
        } else {
            let stderr = String::from_utf8_lossy(&output.stderr).into_owned();
            Err(BumperError::ParseChangelog(
                self.dirname.to_path_buf(),
                stderr,
            ))
        }
    }

    pub fn set_version(&mut self, version: &str) -> Result<String, BumperError> {
        let version = format!("{}-1", version);
        self.dch(&["-v", &version, ""])?;
        self.dch(&["-r", ""])?;
        Ok(version)
    }

    fn dch(&self, args: &[&str]) -> Result<(), BumperError> {
        let output = Command::new("dch")
            .args(args)
            .current_dir(&self.dirname)
            .output()
            .map_err(|err| BumperError::DchInvoke(self.dirname.to_path_buf(), err))?;
        if output.status.success() {
            Ok(())
        } else {
            let stderr = String::from_utf8_lossy(&output.stderr).into_owned();
            Err(BumperError::Dch(self.dirname.to_path_buf(), stderr))
        }
    }
}
