use std::path::PathBuf;

#[derive(Debug, thiserror::Error)]
pub enum BumperError {
    #[error("Tag pattern is not ASCII: {0}")]
    TagPatternNotAscii(String),

    #[error("Can't figure out what kind of project: {0}")]
    UnknownProjectKind(PathBuf),

    #[error("Couldn't read file {0}: {1}")]
    Read(PathBuf, #[source] std::io::Error),

    #[error("Couldn't write file {0}: {1}")]
    Write(PathBuf, #[source] std::io::Error),

    #[error("Couldn't find Cargo.toml in {0}: {1}")]
    CargoTomlNotFound(PathBuf, #[source] cargo_edit::Error),

    #[error("Couldn't parse Cargo.toml from {0}: {1}")]
    FromToml(PathBuf, #[source] cargo_edit::Error),

    #[error("Couldn't create Cargo.toml manifest to {0}: {1}")]
    WriteToml(PathBuf, #[source] cargo_edit::Error),

    #[error("Cargo.toml doesn't have a 'package' table")]
    NoPackage(#[source] cargo_edit::Error),

    #[error("'project' in Cargo.toml is not a table")]
    ProjectNotTable,

    #[error("Cargo.toml doesn't specify a package name")]
    UnnamedProject,

    #[error("Couldn't turn {0} into an absolute path: {1}")]
    AbsPath(PathBuf, #[source] std::io::Error),

    #[error("Failed to run git {1:?} in {0}: {2}")]
    GitInvoke(PathBuf, Vec<String>, #[source] std::io::Error),

    #[error("git {1:?} failed in {0}: {2}")]
    Git(PathBuf, Vec<String>, String),

    #[error("Failed to run dch in {0}: {1}")]
    DchInvoke(PathBuf, #[source] std::io::Error),

    #[error("dch failed in {0}: {1}")]
    Dch(PathBuf, String),

    #[error("Failed to run dpkg-parsechangelog in {0}: {1}")]
    ParseChangelogInvoke(PathBuf, #[source] std::io::Error),

    #[error("dpkg-parsechangelog failed in {0}: {1}")]
    ParseChangelog(PathBuf, String),

    #[error("Failed to run setup.py in {0}: {1}")]
    Setupnvoke(PathBuf, #[source] std::io::Error),

    #[error("setup.py failed in {0}: {1}")]
    Setup(PathBuf, String),

    #[error("Failed to run cargo in {0}: {1}")]
    CargoInvoke(PathBuf, #[source] std::io::Error),

    #[error("cargo failed in {0}: {1}")]
    Cargo(PathBuf, String),

    #[error("can't find any version.py files in Python project at {0}")]
    NoVersionPy(PathBuf),

    #[error("couldn't write Python version to {0}: {1}")]
    PythonWrite(PathBuf, std::io::Error),
}
